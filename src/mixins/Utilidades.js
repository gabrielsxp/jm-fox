import { LocalStorage } from 'quasar'
import store from '../store'

export default {
  data () {
    return {
      favoritos: []
    }
  },
  computed: {
    buscando: {
      get () {
        return store.state.buscando
      }
    }
  },
  watch: {
    buscando (value) {
      if (value) this.$q.loading.show()
      else this.$q.loading.hide()
    }
  },
  mounted () {
    const favoritos = LocalStorage.getItem('favorito')
    this.favoritos = favoritos ? JSON.parse(favoritos) : []
  },
  methods: {
    irRota (name, params = null) {
      if (params) {
        this.$router.push({ name, params })
      } else {
        this.$router.push({ name })
      }
    },

    dinheiroBR (value) {
      return new Intl.NumberFormat('pt-br', { style: 'currency', currency: 'BRL' }).format(value)
    },

    favoritoSubmit (codigo) {
      const index = this.favoritos.indexOf(codigo)
      if (index > -1) this.favoritos.splice(index, 1)
      else this.favoritos.push(codigo)
      LocalStorage.set('favorito', JSON.stringify(this.favoritos))
    },

    findFavorito (codigo) {
      return this.favoritos.find(ele => ele === codigo)
    },

    ordenarArray (a, b) {
      return a < b ? -1 : a > b ? 1 : 0
    },

    ordenacaoAleatoriaParams () {
      const orderAleatorio = Math.floor(Math.random() * 10)
      let order = { DataHoraAtualizacao: 'desc' }

      if (orderAleatorio === 1) {
        order = { DataHoraAtualizacao: 'asc' }
      } else if (orderAleatorio === 2) {
        order = { DataHoraAtualizacao: 'desc' }
      } else if (orderAleatorio === 3) {
        order = { BairroComercial: 'asc' }
      } else if (orderAleatorio === 4) {
        order = { BairroComercial: 'desc' }
      } else if (orderAleatorio === 5) {
        order = { Codigo: 'asc' }
      } else if (orderAleatorio === 6) {
        order = { Categoria: 'asc' }
      } else if (orderAleatorio === 7) {
        order = { Categoria: 'desc' }
      } else {
        order = { Codigo: 'desc' }
      }

      return order
    }
  }
}
