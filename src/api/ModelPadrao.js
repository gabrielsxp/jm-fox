import superagent from 'superagent'
import superagentCache from 'superagent-cache'
import store from '../store'
import camelCase from 'camelcase'
import clone from 'clone'

superagentCache(superagent, {
  backgroundRefresh: false,
  verbose: false,
  defaultExpiration: 1
})

export default class ModelPadrao {
  storeName = ''
  uri = ''
  iniciado = false

  apiUrl = 'https://jonasreg-rest.vistahost.com.br'
  key = '7f356462bd11ec6bd2c9543bb2a332fc'

  async iniciar () {
    if (this.storeName === '') {
      this.storeName = camelCase(this.uri).replace('/', '')
    }

    this.iniciado = true
  }

  async get (params = null) {
    // KEY
    if (params === null) params = {}
    params.key = this.key

    if (!this.iniciado) {
      await this.iniciar()
    }

    const urlUri = clone(this.uri)

    try {
      store.state.buscando = true

      const c = await superagent
        .get(this.apiUrl + urlUri)
        .query(params)
        .set('accept', 'application/json')

      store.state.buscando = false

      if (c && c.body) {
        if (this.storeName && store.state.dados && store.state.dados[this.storeName]) {
          store.state.dados[this.storeName] = []

          var dados = []
          Object.keys(c.body).map((item) => {
            if (item !== 'pagina' && item !== 'paginas' && item !== 'quantidade' && item !== 'total') {
              dados.push(c.body[item])
            }
          })

          store.state.dados[this.storeName] = {
            dados: dados,
            pagina: c.body.pagina ?? 0,
            paginas: c.body.paginas ?? 0,
            quantidade: c.body.quantidade ?? 0,
            total: c.body.total ?? 0
          }
        }

        console.log(`dados store ${this.storeName}: `, store.state.dados[this.storeName])
      } else {
        console.error('url', this.apiUrl + urlUri)
        console.error('nao tava preparado pra esse resultado', c)
      }

      return [0, c]
    } catch (err) {
      console.error('Opaaaaa Deu Erro na Api, que PENINHA')
      store.state.buscando = true

      if (typeof (this.buscarPadrao) !== 'undefined') {
        setTimeout(() => this.buscarPadrao(), 1000)
      }

      return [1, err]
    }
  }
}
