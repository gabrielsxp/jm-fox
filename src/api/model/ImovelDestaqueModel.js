import Padrao from '../ModelPadrao'

export default class ImovelDestaqueModel extends Padrao {
    uri = '/imoveis/listar';
    storeName = 'imoveisDestaque';
}
