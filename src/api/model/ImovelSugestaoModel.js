import Padrao from '../ModelPadrao'

export default class ImovelSugestaoModel extends Padrao {
    uri = '/imoveis/listar';
    storeName = 'imoveisSugestao';
}
