import Padrao from '../ModelPadrao'

export default class ImovelListarModel extends Padrao {
    uri = '/imoveis/listar';
    storeName = 'imoveis';
}
