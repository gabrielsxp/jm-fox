import Header from 'src/components/Header.vue'
import Filtros from 'src/components/Filtros.vue'
import CardImovel from 'src/components/CardImovel.vue'
import ImovelListarModel from 'src/api/model/ImovelListarModel'
import ParamsRequest from 'src/mixins/ParamsRequest'
import Utilidades from 'src/mixins/Utilidades'
import store from 'src/store'

export default {
  name: 'Busca',
  components: { Header, Filtros, CardImovel },
  mixins: [ParamsRequest, Utilidades],
  data () {
    return {
      modelPadrao: new ImovelListarModel(),
      filtros: false,
      ordenar: 'Relevância',

      filterParams: {
        ExibirNoSite: 'Sim',
        Status: ['VENDA', 'VENDA E LOCACAO']
      },

      orderParams: { DataHoraAtualizacao: 'desc' }
    }
  },
  computed: {
    tamanhoJanela () {
      if (process.browser) {
        return window.innerWidth
      } else {
        return 300
      }
    },
    imoveis: {
      get () {
        return (store.state.dados[this.modelPadrao.storeName]) ? store.state.dados[this.modelPadrao.storeName].dados : []
      }
    },
    totalImoveis: {
      get () {
        return (store.state.dados[this.modelPadrao.storeName]) ? store.state.dados[this.modelPadrao.storeName].total : null
      }
    },
    paginas: {
      get () {
        return (store.state.dados[this.modelPadrao.storeName]) ? store.state.dados[this.modelPadrao.storeName].paginas : 0
      }
    },
    paginaAtual: {
      get () {
        return (store.state.dados[this.modelPadrao.storeName]) ? store.state.dados[this.modelPadrao.storeName].pagina : 1
      },
      set (value) {
        store.state.dados[this.modelPadrao.storeName].pagina = value
      }
    }
  },
  watch: {
    '$route.query' (value) {
      if (value && Object.keys(value).length > 0) {
        this.filtroQuery(value)
      } else {
        this.resetFilterParams()
      }
    },
    ordenar (value) {
      if (value) {
        if (value === 'Relevância') {
          this.orderParams = { DataHoraAtualizacao: 'desc' }
        }

        if (value === 'Maior Preço') {
          this.orderParams = { ValorVenda: 'desc' }
        }

        if (value === 'Menor Preço') {
          this.orderParams = { ValorVenda: 'asc' }
        }

        if (value === 'Maior Área') {
          this.orderParams = { AreaPrivativa: 'desc' }
        }

        if (value === 'Menor Área') {
          this.orderParams = { AreaPrivativa: 'asc' }
        }

        this.buscarPadrao()
      }
    }
  },
  mounted () {
    if (this.$route.query && Object.keys(this.$route.query).length > 0) {
      this.filtroQuery(this.$route.query)
    } else {
      this.buscarPadrao()
    }
  },
  methods: {
    buscarPadrao () {
      const params = {
        showtotal: 1,
        pesquisa: JSON.stringify(
          {
            fields: this.fields(),
            filter: this.filterParams,
            order: this.orderParams,
            paginacao: { pagina: this.paginaAtual, quantidade: 12 }
          }
        )
      }
      this.modelPadrao.get(params)
    },

    filtroQuery (obj) {
      if (obj) {
        // string para Array
        Object.keys(obj).map((i) => {
          if (i !== 'order') {
            if (i === 'categoria' || i === 'bairro' || i === 'dormitorio' || i === 'vaga') {
              if (typeof obj[i] === 'string') obj[i] = [obj[i]]
            }
          }
        })
        /* */

        if (obj.status) {
          if (obj.status === 'Comprar') {
            this.filterParams.Status = ['VENDA', 'VENDA E LOCACAO']
          }

          if (obj.status === 'Alugar') {
            this.filterParams.Status = ['LOCACAO']
          }
        }

        if (obj.referencia && obj.referencia.length > 0) {
          this.filterParams.Codigo = obj.referencia
        }

        if (obj.referencia === '') {
          delete this.filterParams.Codigo
        }

        if (obj.bairro) {
          if (obj.bairro.length > 0) {
            this.filterParams.BairroComercial = obj.bairro
          } else {
            delete this.filterParams.BairroComercial
          }
        }

        if (obj.categoria) {
          if (obj.categoria.length > 0) {
            if (this.$refs.filtroComponente) {
              const ref = this.$refs.filtroComponente
              obj.categoria.map((ele, index) => {
                const encontrado = ref.listaCategorias.find(item => item.url === ele)
                if (encontrado) obj.categoria[index] = encontrado.value
              })
            }
            this.filterParams.Categoria = obj.categoria
          } else {
            delete this.filterParams.Categoria
          }
        }

        if (obj.dormitorio) {
          if (obj.dormitorio.length > 0) {
            this.filterParams.Dormitorios = obj.dormitorio
          } else {
            delete this.filterParams.Dormitorios
          }
        }

        if (obj.vaga) {
          if (obj.vaga.length > 0) {
            this.filterParams.Vagas = obj.vaga
          } else {
            delete this.filterParams.Vagas
          }
        }

        // this.filtros = false
        console.log('filtroQuery: ', obj)
        this.buscarPadrao()
      }
    },

    resetFilterParams () {
      this.filterParams = {
        ExibirNoSite: 'Sim',
        Status: ['VENDA', 'VENDA E LOCACAO']
      }
      this.buscarPadrao()
    }
  }
}
