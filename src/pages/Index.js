import CardImovel from 'src/components/CardImovel.vue'
import CardProcura from 'src/components/CardProcura.vue'
import Utilidades from 'src/mixins/Utilidades'
import CarrosselCards from 'src/components/CarrosselCards.vue'
import ImovelDestaqueModel from 'src/api/model/ImovelDestaqueModel'
import CasaSvg from 'src/components/icons/CasaSvg.vue'
import ParamsRequest from '../mixins/ParamsRequest'
import store from '../store'

export default {
  name: 'PageIndex',
  mixins: [ParamsRequest, Utilidades],
  components: { CardImovel, CardProcura, CarrosselCards, CasaSvg },
  data () {
    return {
      modelPadrao: new ImovelDestaqueModel(),
      referencia: ''
    }
  },
  computed: {
    imoveisDestaque: {
      get () {
        return (store.state.dados[this.modelPadrao.storeName]) ? store.state.dados[this.modelPadrao.storeName].dados : []
      }
    }
  },
  mounted () {
    this.buscarImoveisDestaque()
  },
  methods: {
    buscarImoveisDestaque () {
      const params = {
        showtotal: 1,
        pesquisa: JSON.stringify(
          {
            fields: this.fields(),
            filter: {
              EmDestaque: 'Sim',
              ExibirNoSite: 'Sim',
              Status: ['VENDA', 'LOCACAO', 'VENDA E LOCACAO']
            },
            order: { DataHoraAtualizacao: 'desc' },
            paginacao: { pagina: 1, quantidade: 50 }
          }
        )
      }
      this.modelPadrao.get(params)
    },

    buscarReferencia () {
      if (this.referencia && this.referencia.length > 0) {
        this.$router.push({ name: 'busca', query: { referencia: this.referencia } })
      }
    },

    irCategoriaBusca (categoria) {
      this.$router.push({ name: 'busca', query: { categoria: categoria } })
    },

    descer () {
      const el = this.$refs.destaques
      el.scrollIntoView({ behavior: 'smooth' })
    }
  }
}
