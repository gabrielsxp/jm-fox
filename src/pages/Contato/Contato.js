import Header from 'src/components/Header.vue'
import CardImovel from 'src/components/CardImovel.vue'
import CarrosselCards from 'src/components/CarrosselCards.vue'
import ImovelSugestaoModel from 'src/api/model/ImovelSugestaoModel'
import ParamsRequest from 'src/mixins/ParamsRequest'
import Utilidades from 'src/mixins/Utilidades'
import store from 'src/store'

export default {
  name: 'Contato',
  components: { Header, CardImovel, CarrosselCards },
  mixins: [ParamsRequest, Utilidades],
  data () {
    return {
      modelPadrao: new ImovelSugestaoModel()
    }
  },
  computed: {
    imoveisSugestao: {
      get () {
        return (store.state.dados[this.modelPadrao.storeName]) ? store.state.dados[this.modelPadrao.storeName].dados : []
      }
    }
  },
  mounted () {
    this.buscarPadrao()
  },
  methods: {
    buscarPadrao () {
      const params = {
        showtotal: 1,
        pesquisa: JSON.stringify(
          {
            fields: this.fields(),
            filter: {
              ExibirNoSite: 'Sim',
              Status: ['VENDA', 'LOCACAO', 'VENDA E LOCACAO']
            },
            order: this.ordenacaoAleatoriaParams(),
            paginacao: { pagina: 1, quantidade: 10 }
          }
        )
      }

      this.modelPadrao.get(params)
    },

    enviar () {
      //
    },
    validarEmail (email) {
      // eslint-disable-next-line no-useless-escape
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return re.test(String(email).toLowerCase())
    },
    openUrl (url) {
      window.open(url, '_blank')
    }
  }
}
