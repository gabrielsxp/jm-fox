import Header from 'src/components/Header.vue'
import Carrossel from 'src/components/CarrosselAnuncio.vue'
import CarrosselCards from 'src/components/CarrosselCards.vue'
import ImovelDetalhesModel from 'src/api/model/ImovelDetalhesModel'
import ImovelSugestaoModel from 'src/api/model/ImovelSugestaoModel'
import ParamsRequest from 'src/mixins/ParamsRequest'
import CardImovel from 'src/components/CardImovel.vue'
import Utilidades from 'src/mixins/Utilidades'
import store from 'src/store'

import PinMapIcon from 'src/components/icons/PinMapIcon.vue'
import CamaIcon from 'src/components/icons/CamaIcon.vue'
import AreaIcon from 'src/components/icons/AreaIcon.vue'
import BanheiroIcon from 'src/components/icons/BanheiroIcon.vue'
import VagaIcon from 'src/components/icons/VagaIcon.vue'

export default {
  name: 'Anuncio',
  components: { Header, Carrossel, CardImovel, CarrosselCards, PinMapIcon, CamaIcon, AreaIcon, BanheiroIcon, VagaIcon },
  mixins: [ParamsRequest, Utilidades],
  data () {
    return {
      modelPadrao: new ImovelDetalhesModel(),
      modelSugestao: new ImovelSugestaoModel(),
      dados: null,

      whatsapp: false,
      settings: {
        dots: false,
        arrows: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        swipe: true,
        centerMode: false,
        slidesToScroll: 4,
        initialSlide: 0,
        responsive: [
          {
            breakpoint: 1500,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          },
          {
            breakpoint: 1100,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 720,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              swipe: true
            }
          },
          {
            breakpoint: 470,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              swipe: true
            }
          }
        ]
      }
    }
  },
  computed: {
    imoveisSugestao: {
      get () {
        return (store.state.dados[this.modelSugestao.storeName]) ? store.state.dados[this.modelSugestao.storeName].dados : []
      }
    }
  },
  watch: {
    '$route.params.codigo' (agora, antes) {
      if (agora !== antes) {
        this.buscarPadrao()
      }
    }
  },
  mounted () {
    this.buscarPadrao()
  },
  methods: {
    buscarPadrao () {
      const params = {
        imovel: this.$route.params.codigo,
        pesquisa: JSON.stringify({ fields: this.fieldsImovelDetalhes() })
      }

      this.modelPadrao.get(params).then(([err, res]) => {
        if (!err) {
          var obj = res.body

          obj.fotos = []
          obj.caracteristicasImovel = []
          obj.caracteristicasCondominio = []

          // Fotos do Imovel
          if (obj.Foto) {
            Object.keys(obj.Foto).map((item) => {
              if (obj.Foto[item].Foto) {
                obj.fotos.push({ src: obj.Foto[item].Foto, video: null })
              }
            })

            if (obj.VideoDestaque) {
              obj.fotos.push({ src: `https://img.youtube.com/vi/${obj.VideoDestaque}/0.jpg`, video: `https://www.youtube.com/embed/${obj.VideoDestaque}` })
            }
          }

          // Caracteristicas do Imovel
          if (obj.Caracteristicas) {
            Object.keys(obj.Caracteristicas).map((item) => {
              if (obj.Caracteristicas[item] && obj.Caracteristicas[item] === 'Sim') {
                obj.caracteristicasImovel.push(item)
              }
            })
            obj.caracteristicasImovel.sort(this.ordenarArray)
          }

          // Caracteristicas do Condominio
          if (obj.InfraEstrutura) {
            Object.keys(obj.InfraEstrutura).map((item) => {
              if (obj.InfraEstrutura[item] && obj.InfraEstrutura[item] === 'Sim') {
                obj.caracteristicasCondominio.push(item)
              }
            })
            obj.caracteristicasCondominio.sort(this.ordenarArray)
          }

          delete obj.Foto
          delete obj.Caracteristicas
          delete obj.InfraEstrutura

          this.dados = obj

          if (this.dados.BairroComercial) this.buscarSugestoesImovel()
        }
      })
    },

    valorImovel () {
      if (this.dados.Status === 'VENDA') {
        return this.dinheiroBR(this.dados.ValorVenda)
      } else if (this.dados.Status === 'LOCACAO') {
        return this.dinheiroBR(this.dados.ValorLocacao)
      } else if (this.dados.Status === 'VENDA E LOCACAO') {
        return this.dinheiroBR(this.dados.ValorVenda)
      }
      return null
    },

    buscarSugestoesImovel () {
      const params = {
        showtotal: 1,
        pesquisa: JSON.stringify(
          {
            fields: this.fields(),
            filter: {
              BairroComercial: [this.dados.BairroComercial],
              ExibirNoSite: 'Sim',
              Status: ['VENDA', 'LOCACAO', 'VENDA E LOCACAO']
            },
            order: this.ordenacaoAleatoriaParams(),
            paginacao: { pagina: 1, quantidade: 10 }
          }
        )
      }

      this.modelSugestao.get(params).then(([err]) => {
        if (!err) {
          const tirarImovelAberto = this.imoveisSugestao.filter(ele => ele.Codigo !== this.dados.Codigo)
          if (store.state.dados[this.modelSugestao.storeName]) {
            store.state.dados[this.modelSugestao.storeName].dados = tirarImovelAberto
          }
        }
      })
    }
  }
}
