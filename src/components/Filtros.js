import CasaIcon from 'src/components/icons/CasaIcon.vue'
import ListarConteudoModel from 'src/api/model/ListarConteudoModel'
import { LocalStorage } from 'quasar'
import clone from 'clone'

export default {
  name: 'Filtros',
  components: { CasaIcon },
  data () {
    return {
      modelBairro: new ListarConteudoModel(),

      status: 'Comprar',
      referencia: '',
      bairro: [],
      dormitorio: [],
      vaga: [],

      menorpreco: null,
      maiorpreco: null,

      listaCategorias: [
        { label: 'Apartamento', value: 'Apartamentos', url: 'apartamentos', marcado: 0 },
        { label: 'Casa Comercial', value: 'Casa comercial', url: 'casa-comercial', marcado: 0 },
        { label: 'Casa de Vila', value: 'Casa de Vila', url: 'casa-de-vila', marcado: 0 },
        { label: 'Casa em Condomínio', value: 'Casa em Condominio', url: 'casa-em-condominio', marcado: 0 },
        { label: 'Casa', value: 'Casa', url: 'casas', marcado: 0 },
        { label: 'Cobertura', value: 'Coberturas', url: 'coberturas', marcado: 0 }
      ],
      listaBairros: []
    }
  },
  mounted () {
    // Bairros
    const bairros = LocalStorage.getItem('bairros')
    if (bairros) this.listaBairros = JSON.parse(bairros)
    else this.buscarBairro()
    /* */

    this.getQueryParams()
  },
  methods: {
    buscarBairro () {
      const params = { pesquisa: JSON.stringify({ fields: ['BairroComercial'] }) }
      this.modelBairro.get(params).then(([err, res]) => {
        if (!err && res && res.body && res.body.BairroComercial) {
          LocalStorage.set('bairros', JSON.stringify(res.body.BairroComercial))
        }
      })
    },

    categoriaSelecionado () {
      const categoriaMarcado = []

      this.listaCategorias.map((ele) => {
        if (ele.marcado === 1) {
          categoriaMarcado.push(ele.url)
        }
      })

      this.setQueryParams('categoria', categoriaMarcado)
    },

    getQueryParams () {
      var query = clone(this.$route.query)

      if (query && Object.keys(query).length > 0) {
        Object.keys(query).map((i) => {
          if (i !== 'order') {
            // string para Array
            if (i === 'categoria' || i === 'bairro' || i === 'dormitorio' || i === 'vaga') {
              if (typeof query[i] === 'string') query[i] = [query[i]]
            }
            /* */

            if (i === 'categoria') {
              query[i].map((categoriaValue) => {
                const indexListaCategoria = this.listaCategorias.findIndex(item => item.url === categoriaValue)
                if (indexListaCategoria > -1) {
                  this.listaCategorias[indexListaCategoria].marcado = 1
                }
              })
            } else this[i] = query[i]
          }
        })
      }
    },

    setQueryParams (campo, value) {
      if (campo && value) {
        var query = this.$route.query
        query[campo] = value

        console.log('setQueryParams: ', query)

        this.$router.push(this.$route.path).catch(() => {})
        this.$router.push({ name: this.$route.name, query: query })
      }
    },

    limparFiltroQuery () {
      this.status = 'Comprar'
      this.referencia = ''
      this.bairro = []
      this.dormitorio = []
      this.vaga = []

      this.menorpreco = null
      this.maiorpreco = null

      this.listaCategorias.map((ele, index) => {
        this.listaCategorias[index].marcado = 0
      })

      this.$router.push(this.$route.path).catch(() => {})
      this.$router.push({ name: this.$route.name, query: {} })
    }
  }
}
