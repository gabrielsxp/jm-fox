import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'Header',
  mixins: [Utilidades],
  props: {
    rotaAtualCustom: {
      type: String,
      default: null
    },
    historicPag: {
      type: Array,
      default: () => []
    }
  },
  data () {
    return {
      rotaAtual: ''
    }
  },
  watch: {
    rotaAtualCustom (agora, antes) {
      if (agora !== antes) {
        this.inicio()
      }
    }
  },
  mounted () {
    this.inicio()
  },
  methods: {
    inicio () {
      const rota = this.$router.currentRoute.name
      if (this.rotaAtualCustom) {
        this.rotaAtual = this.rotaAtualCustom
      } else {
        this.capitalizar(rota)
      }
    },
    capitalizar (rota) {
      const nome = rota.split('_')
      if (nome.length > 0) {
        for (let i = 0, x = nome.length; i < x; i++) {
          nome[i] = nome[i][0].toUpperCase() + nome[i].substr(1)
        }
        this.rotaAtual = nome.join(' ')
      } else {
        this.rotaAtual = rota
      }
    }
  }
}
