import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'CardProcura',
  mixins: [Utilidades],
  props: {
    tipo: {
      type: String,
      default: 'Casas'
    }
  },
  methods: {
    irCategoriaBusca () {
      this.$router.push({ name: 'busca', query: { categoria: this.tipo.toLowerCase() } })
    }
  }
}
