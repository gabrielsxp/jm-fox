export default {
  name: 'RedesSociais',
  methods: {
    openUrl (url) {
      window.open(url, '_blank')
    }
  }
}
