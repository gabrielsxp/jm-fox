import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'CardImovel',
  mixins: [Utilidades],
  props: {
    dados: {
      type: Object
    }
  },
  methods: {
    valorImovel () {
      if (this.dados.Status === 'VENDA') {
        return this.dinheiroBR(this.dados.ValorVenda)
      } else if (this.dados.Status === 'LOCACAO') {
        return this.dinheiroBR(this.dados.ValorLocacao)
      } else if (this.dados.Status === 'VENDA E LOCACAO') {
        return this.dinheiroBR(this.dados.ValorVenda)
      }
      return null
    },

    irImovel () {
      const nome = `${this.dados.Categoria} ${this.dados.BairroComercial} ${this.dados.Cidade}`
      const nomeUrl = nome.split(' ').join('-').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').trim()
      this.irRota('anuncio', { nome: nomeUrl, codigo: this.dados.Codigo })
    }
  }
}
