import RedesSociais from 'src/components/RedesSociais.vue'
import Footer from 'src/components/Footer.vue'
import Utilidades from 'src/mixins/Utilidades'

export default {
  name: 'MainLayout',
  mixins: [Utilidades],
  computed: {
    classeNavbar () {
      return this.solida ? 'solida' : 'transparente'
    }
  },
  data () {
    return {
      menu: false,
      solida: false,
      scrollInfo: {}
    }
  },
  methods: {
    onScroll (info) {
      if (info.position > 100) {
        this.solida = true
      } else {
        this.solida = false
      }
    },
    btnScrollTop () {
      let classe = 'btn-top'
      if (this.solida) {
        classe += ' aparecer'
      }
      return classe
    },
    irTopo () {
      const topo = this.$refs.topo
      if (topo) {
        this.$nextTick(() => {
          topo.scrollIntoView({ behavior: 'smooth', block: 'start' })
        })
      }
    }
  },
  components: {
    RedesSociais,
    Footer
  }
}
