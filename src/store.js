import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    buscando: true,
    dados: {
      imoveis: { dados: [], pagina: 1, paginas: 0, quantidade: 0, total: 0 },
      imoveisDestaque: { dados: [], pagina: 1, paginas: 0, quantidade: 0, total: 0 },
      imoveisSugestao: { dados: [], pagina: 1, paginas: 0, quantidade: 0, total: 0 }
    },
    keyGoogleMaps: 'AIzaSyBuhnxzzqwd9MY0uCFMaPADndK8kgABVG0'
  },
  mutations: {
    setDados (state, n) {
      state.dados[n.chave] = n.dados
    }
  }
})
